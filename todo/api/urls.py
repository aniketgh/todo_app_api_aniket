from django.urls import path
from .import views


urlpatterns = [
    path('',views.apiOverview,name="apiOverview"),
    path('list/',views.taskList,name="taskList"),
    path('details/<str:pk>/',views.taskDetails,name="taskDetails"),
    path('create/',views.taskCreate,name="taskCreate"),
    path('update/<str:pk>/',views.taskUpdate,name="taskUpdate"),
    path('delete/<str:pk>/',views.taskDelete,name="taskDelete"),
]

